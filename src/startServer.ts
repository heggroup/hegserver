import express from "express";
import { addHandlers } from "./handlers/addHandlers";
import { initialiseUsers } from "./logins";
import { configureApp } from "./app";
import { Server } from "http";
import { info } from "./log";
import fs from "fs";

export const startServer = async (): Promise<Server> => {
  const storageDir = process.env.STORAGE_DIRECTORY!!;
  const userPath = `${storageDir}/users.json`;
  const port = process.env.PORT!!;
  const protectedSuffix = process.env.PROTECTED_SUFFIX!!;
  const masterUser = {
    username: process.env.MASTER_USERNAME!!,
    password: process.env.MASTER_PASSWORD!!,
  };

  if (
    !storageDir ||
    !userPath ||
    !port ||
    !protectedSuffix ||
    !masterUser.username ||
    !masterUser.password
  ) {
    info("Missing key config");
    process.exit(-1);
  }

  if (!fs.existsSync(`${storageDir}/tmp/`)) {
    fs.mkdirSync(`${storageDir}/tmp/`, { recursive: true });
    fs.mkdirSync(`${storageDir}/web/`, { recursive: true });
  }

  const app = express();
  configureApp(app);

  const users = await initialiseUsers(userPath, masterUser);

  addHandlers(
    app,
    users,
    storageDir,
    !!process.env.DISABLE_WEBSERVER,
    protectedSuffix,
    masterUser
  );

  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      reject(`Failed to start server`);
    }, 10000);

    var server: Server | undefined;
    try {
      server = app.listen(port, () => {
        info(`HegServer listening on http://localhost:${port}`);
      });
      clearTimeout(timeout);
      resolve(server);
    } catch (e) {
      server?.close();
      info(`Failed to start server - ${e}`);
      info(` Retrying in 1 second`);
      setTimeout(startServer, 1000);
    }
  });
};
