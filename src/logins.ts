import fs from "fs/promises";
import { info } from "./log";

type Login = { username: string; password: string };
type User = { login: Login; directories: string[] };

const findUser = (login: Login) => (u: User) =>
  u.login.username === login.username && u.login.password === login.password;

export class UserDirectory {
  users: User[];
  persistUsers: () => void;
  masterUser: Login;

  constructor(
    initialUsers: User[],
    persistUsers: (users: User[]) => void,
    masterUser: Login
  ) {
    this.users = initialUsers;
    this.persistUsers = () => persistUsers(this.users);
    this.masterUser = masterUser;
  }

  #isMasterUser(login: Login): boolean {
    return (
      login.username === this.masterUser.username &&
      login.password === this.masterUser.password
    );
  }

  addUser(newUser: Login, login: Login): boolean {
    if (
      this.users.find((u) => u.login.username === newUser.username) ||
      !this.#isMasterUser(login)
    ) {
      return false;
    } else {
      this.users.push({ login: newUser, directories: [] });
      setImmediate(this.persistUsers);
      return true;
    }
  }

  removeUser(login: Login): boolean {
    if (this.users.find(findUser(login))) {
      this.users = this.users.filter((u) => !findUser(login)(u));
      setImmediate(this.persistUsers);
      return true;
    } else {
      return false;
    }
  }

  getUserDirs(login: Login): string[] | undefined {
    return this.users.find(findUser(login))?.directories;
  }

  addUserDir(login: Login, dir: string): boolean {
    const user = this.users.find(findUser(login));
    if (user && !this.users.flatMap((u) => u.directories).includes(dir)) {
      info(`registering directory ${dir} to ${login.username}`);
      user.directories.push(dir);
      setImmediate(this.persistUsers);
      return true;
    } else {
      return false;
    }
  }

  removeUserDir(login: Login, dir: string): boolean {
    const user = this.users.find(findUser(login));
    if (user && user.directories.includes(dir)) {
      user.directories = user.directories.filter((d) => d != dir);
      setImmediate(this.persistUsers);
      return true;
    } else {
      return false;
    }
  }
}

const persistUsers = (path: string) => (users: User[]) => {
  return fs.writeFile(path, JSON.stringify(users, undefined, 2));
};

export const initialiseUsers = (
  path: string,
  masterUser: Login
): Promise<UserDirectory> => {
  info("Initialising Users");
  let fileHandle: fs.FileHandle;
  return fs
    .open(path)
    .then((file) => {
      fileHandle = file;
      return file.readFile();
    })
    .then((buf) => buf.toString("utf8"))
    .then((str) => {
      return JSON.parse(str) as User[];
    })
    .then((users) => new UserDirectory(users, persistUsers(path), masterUser))
    .catch((e: Error) => {
      info(`Error initialising user map - ${e}`);
      return new UserDirectory([], persistUsers(path), masterUser);
    })
    .finally(() => {
      info("User Directory initialised");
      fileHandle?.close();
    });
};
