import fs from "fs/promises";
import fsSync from "fs";
import { info } from "../log";
import { UserDirectory } from "../logins";
import { Request, Response } from "express";
import StreamZip from "node-stream-zip";

const STATUS_404 = "static/404.html";
const STATUS_500 = "static/500.html";

const getNearest404 = (basePath: string, urlPath: string): string => {
  if (!urlPath || urlPath === "/") {
    return STATUS_404;
  }
  const pathBreakdown = urlPath.split("/");
  const parent = pathBreakdown.slice(0, -1).join("/");
  const local404 = `${basePath}${parent}/404.html`;
  if (fsSync.existsSync(local404)) {
    return local404;
  } else {
    return getNearest404(basePath, parent);
  }
};

export const getFile = async (
  basePath: string,
  urlPath: string
): Promise<string> => {
  const path = `${basePath}${urlPath}`;
  return fs
    .lstat(path)
    .then((stats) => {
      if (stats.isDirectory()) {
        if (fsSync.existsSync(`${path}/index.html`)) {
          return `${path}/index.html`;
        } else {
          console.log(basePath, urlPath);
          return getNearest404(basePath, urlPath);
        }
      } else {
        return path;
      }
    })
    .catch((e: Error) => {
      if (e.message.includes("no such file or directory")) {
        info(`404 for ${path}`);
        return getNearest404(basePath, urlPath);
      } else {
        return STATUS_500;
      }
    });
};

export const serveFile =
  (fileDirectory: string) => async (req: Request, res: Response) => {
    try {
      res.sendFile(await getFile(`${fileDirectory}/web`, req.url), {
        root: process.cwd(),
      });
    } catch (e: any) {
      res.sendFile(STATUS_500, {
        root: process.cwd(),
      });
    }
  };

export const postZip =
  (userDirectory: UserDirectory, storageDir: string) =>
  async (req: any, res: Response) => {
    const extractZip = async (file: any, dir: string): Promise<void> => {
      return fs
        .writeFile(`${storageDir}/tmp${dir}.zip`, new Uint8Array(file.buffer))
        .then(() => {
          const zip = new StreamZip.async({
            file: `${storageDir}/tmp${dir}.zip`,
          });
          const targetDir = `${storageDir}/web${dir}`;

          return fs
            .rm(targetDir, { recursive: true, force: true })
            .then(() => fs.mkdir(targetDir, { recursive: true }))
            .then(() => zip.extract(null, targetDir))
            .then((fileCount) =>
              info(`Extracted ${fileCount} files to ${targetDir}`)
            )
            .finally(() =>
              setImmediate(() => fs.rm(`${storageDir}/tmp${dir}.zip`))
            );
        });
    };

    const {
      file,
      body: { username, password },
    } = req;
    if (
      userDirectory
        .getUserDirs({ username, password })
        ?.includes(req.url.substring(1))
    ) {
      info("unpacking zip");
      if (!file) {
        res.status(400).send();
      } else {
        setImmediate(() => extractZip(file, req.url));
        res.status(201).send();
      }
    } else {
      res.status(401).send();
    }
  };
