import { getFile } from "./files";

const dir = describe("File Server", () => {
  test("Returns 404 for non existant file", async () => {
    expect((await getFile("asdfg/", "qwerty")).includes("404")).toBeTruthy();
  });

  test("Returns file if it exists", async () => {
    expect(
      (await getFile("tests/static/web", "/testfile.html")).includes(
        "testfile.html"
      )
    ).toBeTruthy();
  });

  test("Returns index.html for directory", async () => {
    expect(
      (await getFile("tests/static/web", "/someDirectory")).includes(
        "index.html"
      )
    ).toBeTruthy();
  });
});
