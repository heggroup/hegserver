import { Express } from "express";
import { healthcheck, version, shutdown } from "./base";
import { postZip, serveFile } from "./files";
import { UserDirectory } from "../logins";
import { createUser, deleteUser, addDir, removeDir } from "./loginHandler";
import multer from "multer";
import { info } from "../log";

export const addHandlers = (
  app: Express,
  users: UserDirectory,
  storageDir: string,
  disableWebserver: boolean,
  protectedSuffix: string,
  masterUser: { username: string; password: string }
) => {
  app.get(`/${protectedSuffix}/health`, healthcheck);
  app.get(`/${protectedSuffix}/version`, version);
  app.post(`/${protectedSuffix}/shutdown`, shutdown(masterUser));

  app.post(`/${protectedSuffix}/users`, createUser(users));
  app.delete(`/${protectedSuffix}/users`, deleteUser(users));

  app.post(`/${protectedSuffix}/dirs/:dir`, addDir(users));
  app.delete(`/${protectedSuffix}/dirs/:dir`, removeDir(users));

  if (!disableWebserver) {
    app.get("*", serveFile(`${storageDir}`));
  } else {
    info("Webserver disabled.");
  }
  app.post("*", multer().single("zip"), postZip(users, `${storageDir}`));
};
