import { Request, Response } from "express";
import { info } from "../log";
import { UserDirectory } from "../logins";

const statusWrapper =
  (
    operation: (
      login: { username: string; password: string },
      req: Request
    ) => boolean
  ) =>
  (req: Request, res: Response) => {
    try {
      if (!req.body) {
        res.status(400).send();
      } else {
        const { username, password } = req.body;
        if (!username || !password) {
          res.status(400).send();
        } else {
          const success = operation({ username, password }, req);
          if (success) {
            res.status(200).send();
          } else {
            res.status(401).send();
          }
        }
      }
    } catch (e: any) {
      info(e.toString());
    }
  };

export const createUser = (users: UserDirectory) =>
  statusWrapper((login, req: Request) =>
    users.addUser(
      {
        username: req.body.newUsername,
        password: req.body.newPassword,
      },
      login
    )
  );

export const deleteUser = (users: UserDirectory) =>
  statusWrapper((login) => users.removeUser(login));

export const addDir = (users: UserDirectory) =>
  statusWrapper((login, req: Request) =>
    users.addUserDir(login, req.params.dir)
  );

export const removeDir = (users: UserDirectory) =>
  statusWrapper((login, req: Request) =>
    users.removeUserDir(login, req.params.dir)
  );
