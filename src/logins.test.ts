import { UserDirectory } from "./logins";

const masterUser = { username: "master", password: "key" };

describe("UserDirectory", () => {
  test("getUserDirectories returns directories for existing user", () => {
    expect(
      new UserDirectory(
        [
          {
            login: { username: "username", password: "password" },
            directories: ["dir"],
          },
        ],
        () => {},
        masterUser
      ).getUserDirs({ username: "username", password: "password" })
    ).toEqual(["dir"]);
  });

  test("getUserDirectories returns undefined for non-existing user", () => {
    expect(
      new UserDirectory(
        [
          {
            login: { username: "username", password: "password" },
            directories: ["dir"],
          },
        ],
        () => {},
        masterUser
      ).getUserDirs({ username: "username2", password: "password1" })
    ).toBeUndefined();
  });

  test("addUser adds Users", () => {
    const users = new UserDirectory([], () => {}, masterUser);
    expect(
      users.addUser(
        { username: "username2", password: "password1" },
        masterUser
      )
    ).toBeTruthy();
    expect(
      users.getUserDirs({ username: "username2", password: "password1" })
    ).toEqual([]);
  });

  test("addUser fails for wrong login", () => {
    const users = new UserDirectory([], () => {}, masterUser);
    expect(
      users.addUser(
        { username: "username2", password: "password1" },
        { username: "junk", password: "junk" }
      )
    ).toBeFalsy();
    expect(
      users.getUserDirs({ username: "usernam2", password: "password1" })
    ).toBeUndefined();
  });

  test("cannot add existing user", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: ["dir"],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.addUser({ username: "username", password: "password" }, masterUser)
    ).toBeFalsy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toEqual(["dir"]);
  });

  test("removeUser removes user", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: ["dir"],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.removeUser({ username: "username", password: "password" })
    ).toBeTruthy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toBeUndefined();
  });

  test("removeUser cannot remove non-existant user", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: ["dir"],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.removeUser({ username: "username2", password: "password" })
    ).toBeFalsy();
  });

  test("removeUser fails if password is wrong", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: ["dir"],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.removeUser({ username: "username", password: "wrong-password" })
    ).toBeFalsy();
  });

  test("addUserDir lets you register a new directory", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: [],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.addUserDir({ username: "username", password: "password" }, "dir")
    ).toBeTruthy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toEqual(["dir"]);
  });

  test("addUserDir does not let you register an existing directory", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: [],
        },
        {
          login: { username: "username2", password: "password2" },
          directories: ["dir"],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.addUserDir({ username: "username", password: "password" }, "dir")
    ).toBeFalsy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toEqual([]);
  });

  test("addUserDir fails if password is wrong", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: [],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.addUserDir(
        { username: "username", password: "wrong-password" },
        "dir"
      )
    ).toBeFalsy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toEqual([]);
  });

  test("addUserDir fails for non existant user", () => {
    const users = new UserDirectory([], () => {}, masterUser);
    expect(
      users.addUserDir({ username: "username", password: "password" }, "dir")
    ).toBeFalsy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toBeUndefined();
  });

  test("removeUserDir fails for non existant user", () => {
    const users = new UserDirectory([], () => {}, masterUser);
    expect(
      users.addUserDir({ username: "username", password: "password" }, "dir")
    ).toBeFalsy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toBeUndefined();
  });

  test("removeUserDir fails for directory you don't own", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: ["dir"],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.removeUserDir(
        { username: "username", password: "password" },
        "another-dir"
      )
    ).toBeFalsy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toEqual(["dir"]);
  });

  test("removeUserDir lets you remove a directory you own", () => {
    const users = new UserDirectory(
      [
        {
          login: { username: "username", password: "password" },
          directories: ["dir"],
        },
      ],
      () => {},
      masterUser
    );
    expect(
      users.removeUserDir({ username: "username", password: "password" }, "dir")
    ).toBeTruthy();
    expect(
      users.getUserDirs({ username: "username", password: "password" })
    ).toEqual([]);
  });
});
