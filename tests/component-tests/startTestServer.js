import { config } from "dotenv";
import fs from "fs";

export default async function startTestServer() {
  config({ path: "local.env", debug: true });

  const dir = process.env.STORAGE_DIRECTORY;

  if (fs.existsSync(dir)) {
    fs.rmdirSync(dir, { recursive: true, force: true });
  }
  fs.mkdirSync(dir);
  fs.cpSync("tests/static/web", `${dir}/web`, { recursive: true });

  const { startServer } = require("../../dist/startServer.js"); // needs to be imported after dotenv

  await startServer().then(
    (server) =>
      new Promise((resolve, reject) => {
        let lastStatusCode = "None";

        let timeout;
        let task;

        timeout = setTimeout(() => {
          clearInterval(task);
          reject(`No 200 response. last status: ${lastStatusCode}`);
        }, 3000);

        task = setInterval(() => {
          fetch("http://localhost:3000/hegserver/health")
            .then((res) => {
              lastStatusCode = res.status.toString();
              if (res.status === 200) {
                clearTimeout(timeout);
                clearInterval(task);
                resolve(server);
              }
            })
            .catch((e) => console.log(`Error in server healthcheck ${e}`));
        }, 500);
      })
  );
}
