import { generateUser, masterUser } from "../utils/users";

describe.only("Login Handlers", () => {
  it("Returns 200 for user creation", async () => {
    const { username, password } = generateUser("login1");
    const responseCode = await fetch("http://localhost:3000/hegserver/users", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        newUsername: username,
        newPassword: password,
        ...masterUser,
      }),
    }).then((res) => res.status);
    return expect(responseCode).toBe(200);
  });

  it("Returns 401 when trying to create an existing user", async () => {
    const { username, password } = generateUser();

    const responseCode = await fetch("http://localhost:3000/hegserver/users", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        newUsername: username,
        newPassword: password,
        ...masterUser,
      }),
    }).then((res) => res.status);
    expect(responseCode).toBe(200);

    const secondResponseCode = await fetch(
      "http://localhost:3000/hegserver/users",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({
          newUsername: username,
          newPassword: "blah",
          ...masterUser,
        }),
      }
    ).then((res) => res.status);
    return expect(secondResponseCode).toBe(401);
  });

  it("Can delete user", async () => {
    const user = generateUser();

    const responseCode = await fetch("http://localhost:3000/hegserver/users", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        newUsername: user.username,
        newPassword: user.password,
        ...masterUser,
      }),
    }).then((res) => res.status);
    expect(responseCode).toBe(200);

    const secondResponseCode = await fetch(
      "http://localhost:3000/hegserver/users",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "DELETE",
        body: JSON.stringify(user),
      }
    ).then((res) => res.status);
    return expect(secondResponseCode).toBe(200);
  });

  it("Returns 401 when deleting with wrong password", async () => {
    const user = generateUser();

    const responseCode = await fetch("http://localhost:3000/hegserver/users", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        newUsername: user.username,
        newPassword: user.password,
        ...masterUser,
      }),
    }).then((res) => res.status);
    expect(responseCode).toBe(200);

    const secondResponseCode = await fetch(
      "http://localhost:3000/hegserver/users",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "DELETE",
        body: JSON.stringify({ username: user.username, password: "potato" }),
      }
    ).then((res) => res.status);
    return expect(secondResponseCode).toBe(401);
  });
});
