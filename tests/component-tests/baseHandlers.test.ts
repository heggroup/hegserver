describe("Base Handler", () => {
  it("Returns 200 for healthcheck", async () => {
    const responseCode = await fetch(
      "http://localhost:3000/hegserver/health"
    ).then((res) => res.status);
    return expect(responseCode).toBe(200);
  });

  it("Returns version from version endpoint", async () => {
    const response = await fetch("http://localhost:3000/hegserver/version");
    expect(response.status).toBe(200);
    return expect(await response.json().then((j) => Object.keys(j))).toContain(
      "version"
    );
  });
});
