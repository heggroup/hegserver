import { readFileSync } from "fs";
import { pollUrl } from "../utils/requests";
import { generateUser, masterUser } from "../utils/users";

describe("File Upload", () => {
  const { username, password } = generateUser("uploadZip");
  const zipFormData = new FormData();
  zipFormData.append(
    "zip",
    new Blob([readFileSync("tests/static/test.zip")]),
    "zip"
  );
  zipFormData.append("username", username);
  zipFormData.append("password", password);

  beforeAll(async () => {
    const responseCode = await fetch("http://localhost:3000/hegserver/users", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        newUsername: username,
        newPassword: password,
        ...masterUser,
      }),
    }).then((res) => res.status);
    expect(responseCode).toBe(200);
    const secondResponseCode = await fetch(
      "http://localhost:3000/hegserver/dirs/upload",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({ username, password }),
      }
    ).then((res) => res.status);
    return expect(secondResponseCode).toBe(200);
  });

  it("returns 201 for zip upload and then serves zipfile", async () => {
    const responseCode = await fetch("http://localhost:3000/upload", {
      method: "POST",
      body: zipFormData,
    }).then((res) => res.status);
    expect(responseCode).toBe(201);

    return pollUrl("http://localhost:3000/upload", async (response) =>
      (await (await response.blob()).text()).includes("zip-index.html")
    );
  });

  it("returns 401 for zip upload on wrong dir", async () => {
    const responseCode = await fetch("http://localhost:3000/upload-blah", {
      method: "POST",
      body: zipFormData,
    }).then((res) => res.status);
    return expect(responseCode).toBe(401);
  });

  it("returns 400 for no zip", async () => {
    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    const responseCode = await fetch("http://localhost:3000/upload", {
      method: "POST",
      body: formData,
    }).then((res) => res.status);
    return expect(responseCode).toBe(400);
  });
});
