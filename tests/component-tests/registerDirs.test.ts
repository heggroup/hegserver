import { masterUser } from "../utils/users";

const user = { username: "uploader", password: "T35t1ng15c00l" };

describe("File Upload", () => {
  beforeAll(async () => {
    await fetch("http://localhost:3000/hegserver/users", {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        newUsername: user.username,
        newPassword: user.password,
        ...masterUser,
      }),
    }).then((res) => expect(res.status).toBe(200));
  });

  it("returns 200 when registering a new dir", async () => {
    const responseCode = await fetch(
      "http://localhost:3000/hegserver/dirs/testDir",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(user),
      }
    ).then((res) => res.status);
    return expect(responseCode).toBe(200);
  });

  it("returns 401 when registering a new dir for non-existant user", async () => {
    const responseCode = await fetch(
      "http://localhost:3000/hegserver/dirs/testDir",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({ username: "someDude", password: "letMeIn?" }),
      }
    ).then((res) => res.status);
    return expect(responseCode).toBe(401);
  });

  it("returns 200 when deleting dir", async () => {
    const responseCode = await fetch(
      "http://localhost:3000/hegserver/dirs/deleteDir",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(user),
      }
    ).then((res) => res.status);
    expect(responseCode).toBe(200);

    const deleteResponseCode = await fetch(
      "http://localhost:3000/hegserver/dirs/deleteDir",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "DELETE",
        body: JSON.stringify(user),
      }
    ).then((res) => res.status);
    return expect(deleteResponseCode).toBe(200);
  });

  it("cannot delete another users dir", async () => {
    const responseCode = await fetch(
      "http://localhost:3000/hegserver/dirs/myDir",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(user),
      }
    ).then((res) => res.status);
    expect(responseCode).toBe(200);

    const deleteResponseCode = await fetch(
      "http://localhost:3000/hegserver/dirs/myDir",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "DELETE",
        body: JSON.stringify({
          username: "someoneElse",
          password: "Password1",
        }),
      }
    ).then((res) => res.status);
    return expect(deleteResponseCode).toBe(401);
  });
});
