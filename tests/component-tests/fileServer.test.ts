describe("File Server", () => {
  it("returns file", async () => {
    const response = await fetch("http://localhost:3000/testfile.html");
    expect(response.status).toBe(200);
    return expect(await (await response.blob()).text()).toContain(
      "testfile.html"
    );
  });

  it("defaults to index.html", async () => {
    const response = await fetch("http://localhost:3000/someDirectory");
    expect(response.status).toBe(200);
    return expect(await (await response.blob()).text()).toContain("index.html");
  });

  it("returns 404 for non-existant file", async () => {
    const response = await fetch(
      "http://localhost:3000/someDirectoryTheDoesntExist"
    );
    expect(response.status).toBe(200);
    return expect(await (await response.blob()).text()).toContain("404");
  });

  it("returns local 404 for non-existant file witihin directory", async () => {
    const response = await fetch(
      "http://localhost:3000/someDirectory/someFileThatDoesntExist"
    );
    expect(response.status).toBe(200);
    return expect(await (await response.blob()).text()).toContain(
      "directory-404.html"
    );
  });

  it("does not allow you to access parent directories", async () => {
    const response = await fetch("http://localhost:3000/../../package.json");
    expect(response.status).toBe(200);
    return expect(await (await response.blob()).text()).toContain("404");
  });
});
