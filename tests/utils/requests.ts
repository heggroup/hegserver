export const pollUrl = async (
  url: string,
  matcher: (res: Response) => Promise<boolean>
): Promise<Response> => {
  return new Promise((resolve, reject) => {
    const getUrl = async () => {
      fetch(url).then(async (res) => {
        if (await matcher(res)) {
          resolve(res);
        }
      });
    };

    getUrl();
    setTimeout(getUrl, 50);
    setTimeout(reject, 1000);
  });
};
