import { v4 } from "uuid";

export const masterUser = {
  username: process.env.MASTER_USERNAME!!,
  password: process.env.MASTER_PASSWORD!!,
};

export const generateUser = (name?: string | undefined) => {
  const id = name ?? v4();
  return {
    username: `testUser-${id}`,
    password: `password-${id}`,
  };
};
