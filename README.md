# HegServer

Hegserver is a service designed to host multiple web projects. It simply takes zip files it is posted and serves them and handles permissions around different directories.

## Developing

Install the dependencies using

```
yarn install
```

and then start the service with

```
yarn start
```

tests can be run with

```
yarn test
```